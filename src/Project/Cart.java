package Project;
import java.util.HashMap;

public class Cart 
{
	// <restaurantId, <mealname, count>>
	private HashMap<Integer, HashMap<String, Integer>> cartItems;

	public Cart() 
	{
		cartItems = new HashMap<Integer, HashMap<String, Integer>>();
	}

	public void addItemToCart(Integer restaurantId, String item, Integer count) 
	{
		// Check if food from the hotel already exists in the cart
		if (cartItems.containsKey(restaurantId)) 
		{
			// Get food items from the hotel
			HashMap<String, Integer> itemsFromHotel = cartItems.get(restaurantId);
			
			// Check if the same food was previously added
			if (itemsFromHotel.containsKey(item)) 
			{
				itemsFromHotel.put(item, itemsFromHotel.get(item) + count);
			} 
			else 
			{
				itemsFromHotel.put(item, count);
			}
		} 
		else 
		{
			HashMap<String, Integer> food = new HashMap<String, Integer>();
			food.put(item, count);
			cartItems.put(restaurantId, food);
		}
	}

	public void deleteItemFromCart(Integer restaurantId, String item) 
	{
		// Check if food from the hotel already exists in the cart  
		if (!cartItems.containsKey(restaurantId)) 
		{
			return;
		}
		
		// Check if food item is present in the items list of the hotel
		HashMap<String, Integer> itemsFromHotel = cartItems.get(restaurantId);
		if (!itemsFromHotel.containsKey(item)) 
		{
			return;
		}
		
		itemsFromHotel.remove(item);

		// Check and update hotel entries
		if(itemsFromHotel.size() == 0)
		{
			cartItems.remove(restaurantId);
		}
	}

	public void updateItemInCart(Integer restaurantId, String item, Integer count)
	{
		// Check if food from the hotel already exists in the cart
		if (cartItems.containsKey(restaurantId) == false) 
		{
			return;
		}
		
		// Get food items from the hotel
		HashMap<String, Integer> itemsFromHotel = cartItems.get(restaurantId);
		
		// Check if the same food was previously added
		if (itemsFromHotel.containsKey(item)) 
		{
			itemsFromHotel.put(item, count);
		} 
		
		if(itemsFromHotel.get(item) == 0)
		{
			itemsFromHotel.remove(item);
		}

		// Check and update hotel entries
		if(itemsFromHotel.size() == 0)
		{
			cartItems.remove(restaurantId);
		}
	}
	
	public HashMap<Integer, HashMap<String, Integer>> getCartItems() 
	{
		return cartItems;
	}
}
