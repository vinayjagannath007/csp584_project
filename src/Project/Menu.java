package Project;
import java.util.ArrayList;

public class Menu implements java.io.Serializable 
{	
	public Integer RestaurantId;
	public ArrayList<MenuItem> menuItems;
	
	public Menu(Integer id) 
	{
		RestaurantId = id;
		menuItems = new ArrayList<MenuItem>();
	} 
}